import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurantInfosPageRoutingModule } from './restaurant-infos-routing.module';

import { RestaurantInfosPage } from './restaurant-infos.page';
import { LayoutModule } from '../layout/layout.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurantInfosPageRoutingModule,
    LayoutModule
  ],
  declarations: [RestaurantInfosPage]
})
export class RestaurantInfosPageModule { }
