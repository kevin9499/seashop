import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecipeInfosPageRoutingModule } from './recipe-infos-routing.module';

import { RecipeInfosPage } from './recipe-infos.page';
import { LayoutModule } from '../layout/layout.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecipeInfosPageRoutingModule,
    LayoutModule
  ],
  declarations: [RecipeInfosPage]
})
export class RecipeInfosPageModule { }
