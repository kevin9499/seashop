import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BoatInfosPageRoutingModule } from './boat-infos-routing.module';

import { BoatInfosPage } from './boat-infos.page';
import { LayoutModule } from '../layout/layout.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BoatInfosPageRoutingModule,
    LayoutModule
  ],
  declarations: [BoatInfosPage]
})
export class BoatInfosPageModule { }
