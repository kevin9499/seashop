from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse
from fastapi.responses import FileResponse
import os

app = FastAPI()
origins = ["*"]
img_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "static/")

app.add_middleware(
    CORSMiddleware, allow_origins=origins, allow_methods=["*"], allow_headers=["*"]
)


@app.get("/")
def read_root():
    response = RedirectResponse(url="/docs")
    return response


@app.get("/img/")
def get_img(img_name: str = ""):
    return FileResponse(f"{img_path}{img_name}")


@app.get("/boats")
def get_boats(request: Request):
    return [
        {
            "id": "0",
            "name": "De la Brise",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=boat_1.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
        {
            "id": "1",
            "name": "Saphir",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=boat_2.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
        {
            "id": "2",
            "name": "Micher-Gast",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=boat_3.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
        {
            "id": "3",
            "name": "Aquilon",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=boat_4.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
    ]


@app.get("/products")
def get_products(request: Request):
    return [
        {
            "id": 12,
            "name": "Aile de raie",
            "category": 0,
            "price": 10.0,
            "unit": "kg",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "Pêche à la corde",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=aile_de_raie.jpg",
        },
        {
            "id": 9,
            "name": "Araignées",
            "category": 2,
            "price": 7.0,
            "unit": "kg",
            "availability": False,
            "sale": False,
            "discount": 0.0,
            "comments": "Hors saison,  pêche aux casiers",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=araignee.jpg",
        },
        {
            "id": 3,
            "name": "Bar de ligne",
            "category": 0,
            "price": 30.0,
            "unit": "kg",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "Plus de 1.5kg le poisson",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=bar_de_ligne.jpg",
        },
        {
            "id": 2,
            "name": "Bar de ligne portion",
            "category": 0,
            "price": 10.0,
            "unit": "pièce",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "Environ 550-700g la pièce",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=bar_portion.jpg",
        },
        {
            "id": 10,
            "name": "Bouquets cuits",
            "category": 1,
            "price": 8.0,
            "unit": "100g",
            "availability": False,
            "sale": False,
            "discount": 0.0,
            "comments": "Hors saison, pêche à l'épuisette",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=bouquet_cuit.jpg",
        },
        {
            "id": 1,
            "name": "Filet Bar de ligne",
            "category": 0,
            "price": 7.0,
            "unit": "2 filets",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "environ 300g",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=bar_portion.jpg",
        },
        {
            "id": 5,
            "name": "Filet Julienne",
            "category": 0,
            "price": 19.0,
            "unit": "kg",
            "availability": False,
            "sale": False,
            "discount": 0.0,
            "comments": "Pêche à la corde",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=filet_julienne.jpg",
        },
        {
            "id": 7,
            "name": "Huitres N°2 St Vaast",
            "category": 1,
            "price": 9.5,
            "unit": "Dz",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=huitre_stvaast.jpg",
        },
        {
            "id": 8,
            "name": "Huitres N°2 St Vaast",
            "category": 1,
            "price": 38.0,
            "unit": "4 Dz",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=huitre_stvaast.jpg",
        },
        {
            "id": 13,
            "name": "Huîtres N°2 OR St Vaast",
            "category": 1,
            "price": 12.0,
            "unit": "Dz",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "Médaille d'or Salon Agriculture",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=huitre_stvaast.jpg",
        },
        {
            "id": 14,
            "name": "Huîtres N°2 OR St Vaast",
            "category": 1,
            "price": 24.0,
            "unit": "2 Dz",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "Médaille d'or salon agriculture",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=huitre_stvaast.jpg",
        },
        {
            "id": 15,
            "name": "Huîtres N°2 OR St Vaast",
            "category": 1,
            "price": 48.0,
            "unit": "4 Dz",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "Médaille d'or salon agriculture",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=huitre_stvaast.jpg",
        },
        {
            "id": 16,
            "name": "Huîtres N°2 St Vaast",
            "category": 1,
            "price": 19.0,
            "unit": "2 Dz",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=huitre_stvaast.jpg",
        },
        {
            "id": 4,
            "name": "Lieu jaune de ligne",
            "category": 0,
            "price": 12.0,
            "unit": "kg",
            "availability": True,
            "sale": False,
            "discount": 0.0,
            "comments": "Environ 550-700g la portion",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=lieu_jaune.jpg",
        },
        {
            "id": 6,
            "name": "Moules de pêche",
            "category": 1,
            "price": 7.0,
            "unit": "kg",
            "availability": True,
            "sale": True,
            "discount": 5.0,
            "comments": "",
            "owner": "tig",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=moules.jpg",
        },
    ]


@app.get("/restaurants")
def get_restaurants(request: Request):
    return [
        {
            "id": "0",
            "name": "Bistrot des Gascons",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=bistrot_gascons.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
        {
            "id": "1",
            "name": "Les fous de lile",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=fous_de_lile.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
        {
            "id": "2",
            "name": "Bistrot Landais",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=bistrot_landais.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
        {
            "id": "3",
            "name": "Villa 9-Trois",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=villa_neuftrois.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
        {
            "id": "4",
            "name": "Bistrot du Sommelier",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=bistrot_sommelier.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
    ]


@app.get("/recipes")
def get_recipes():
    return [
        {
            "id": "0",
            "name": "Homard",
            "title": "Homard au chaud-frois",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=homard_chaudfroid.jpg",
            "description": [
                "Fates cure les homards dans de leau bout ante see da firm, a laurier, du sel et du porre de Cafenne. Lato",
                "cure 20 min'es. tocottez-les et lausez-les refrovar.",
                "vicoogez irs contres ces homires canis le sins de le anguew.",
                "leers et la censolette eidoe",
            ],
        },
        {
            "id": "1",
            "name": "St Jacques",
            "title": "Noix de Saint-Jacques flambé au cognac",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=noix_sj_cognac.jpg",
            "description": [
                "fine fondre du beurne asso des échaiches pais a oster lea noce de Sant-vaopoce. Lec faire recenir en Laiceant",
                "mika fransivade put its retror du",
                "a jouler / all el le persi dans la pole el lisser our e coelgoes secondes., ben tare cabster a pole, pus force",
                "da Cosasa, Une fola la fisema Steinte, a loctar les noix de Saint Vacçoes Il ne fast pas les flamber car eves",
                "Wigutter chagd Astire de sssemessne done fondas de pore sor",
            ],
        },
        {
            "id": "2",
            "name": "Bar",
            "title": "Bar rôti au laurier frais",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=bar_roti.jpg",
            "description": [
                "Sur une plaque cu un plat slant sa lour, disposer pot pots froties de sorer frais, verser on fret d huse done e",
                "du gros sel Lisposer le bar, poss farroser din flet d bore doive et mettre us pro de oros so sar la pcau",
                "Cote au four pandant 12 mh $ 240°C.",
            ],
        },
        {
            "id": "3",
            "name": "Tourteau",
            "title": "Tourteau linguine",
            "picture": f"http://bateauthibault.studiopixidream.com/img/?img_name=tourteau_linguine.jpg",
            "description": [
                "Qu'il est chaud le soleil",
                "Quand nous sommes en vacances",
                "c'est le sud de la France",
                "Papa bricole au garage",
                "Maman lit dans la chaise longue",
                "Dans ce joli paysage",
                "Moi, je me balade en tongs",
                "Que de bonheur !",
                "Que de bonheur !",
            ],
        },
    ]
