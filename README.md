<div align="center">
  <img src="./resources/assets/logo/wave.png" height="128" />
  <br />
  <h1>Bateau Thibault</h1>
  <blockquote>
  <p>Site / Application Bateau Thibault made with ionic and Angular</p>
  </blockquote>
  <br />
  <img src="https://forthebadge.com/images/badges/built-with-love.svg" />
  <img src="https://forthebadge.com/images/badges/built-for-android.svg" />
  <br />
  <img src="https://forthebadge.com/images/badges/made-with-typescript.svg">
</div>

---

## About

Projet pour le CFA INSTA.

Groupe:
- Lefebvre Kevin
- Maria Raphael
- Lavigne Marbach Francois

## Documentation

Rapport: *Voir le PDF dans le dossier*  
Documentation  
Powerpoint: [Liens vers le Canva](https://www.canva.com/design/DAEvEM0qUEI/yqcz2zQHqraNBrKFHht3Rw/view?utm_content=DAEvEM0qUEI&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)